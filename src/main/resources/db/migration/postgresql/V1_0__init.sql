-- market
create table if not exists market (
  id serial primary key,
  epic varchar(30) unique not null,
  market_name varchar(30) unique not null,
  instrument_type varchar(20) not null,
  expiry varchar(10) not null,
  delay int not null,
  last_open timestamp
);

alter table market add constraint uq_epic unique (epic);
alter table market add constraint uq_name_ticker unique (market_name, expiry);

create index idx_epic on market (epic);
create index idx_name on market (market_name);
create index idx_expiry on market (expiry);
create index idx_instrument_type on market (instrument_type);
create index idx_last_open on market (last_open);


create table price_time (
  id serial primary key,
  snapshot_time timestamp not null,
  open_price_bid decimal(12,5) not null,
  open_price_ask decimal(12,5) not null,
  close_price_bid decimal(12,5) not null,
  close_price_ask decimal(12,5) not null,
  high_price_bid decimal(12,5) not null,
  high_price_ask decimal(12,5) not null,
  low_price_bid decimal(12,5) not null,
  low_price_ask decimal(12,5) not null,
  last_traded_volume bigint,
  market_id bigint
);

alter table price_time add constraint uq_market_time unique (market_id, snapshot_time);
alter table price_time add constraint fk_market foreign key (market_id) references market (id);

create index idx_time on price_time (snapshot_time);



