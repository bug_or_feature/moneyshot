package net.bugorfeature.moneyshot.history.session;

import javax.annotation.PostConstruct;
import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import com.iggroup.webapi.samples.client.RestAPI;
import com.iggroup.webapi.samples.client.rest.AuthenticationResponseAndConversationContext;
import com.iggroup.webapi.samples.client.rest.ConversationContext;
import com.iggroup.webapi.samples.client.rest.ConversationContextV3;
import com.iggroup.webapi.samples.client.rest.dto.session.createSessionV3.CreateSessionV3Request;
import com.iggroup.webapi.samples.client.rest.dto.session.refreshSessionV1.RefreshSessionV1Request;

/**
 * Maintains an active authentictated IG session, for use by other services
 *
 * @author Andy Geach
 */
@Service("igSessionService")
@Transactional
public class IgSessionService {

    private static final Logger LOG = LoggerFactory.getLogger(IgSessionService.class);

    private RestAPI restApi;

    private AuthenticationResponseAndConversationContext authenticationContext = null;

    @Value("${spring.ig.username}")
    protected String igUsername;

    @Value("${spring.ig.password}")
    protected String igPassword;

    @Value("${spring.ig.appKey}")
    protected String igAppKey;

    @Autowired
    public IgSessionService(RestAPI restApi) {
        this.restApi = restApi;
    }

    public IgSessionService(RestAPI restApi, AuthenticationResponseAndConversationContext authenticationContext) {
        this.restApi = restApi;
        this.authenticationContext = authenticationContext;
    }

    @PostConstruct
    private void startup() {
        service();
    }

    @Scheduled(cron = "${moneyshot.service.session.cron}", zone = "Europe/London")
    public void service() {

        try {
            if (authenticationContext == null) {
                getToken();
            } else {
                refreshToken();
            }

        } catch (Exception e) {
            LOG.error("Unexpected error: {}", e);

            LOG.info("Resetting auth context");
            authenticationContext = null;

        }
    }

    @Scheduled(cron = "${moneyshot.service.session.check.cron}", zone = "Europe/London")
    void checkContext() {

        LOG.debug("Checking auth context: {}", authenticationContext == null);

        try {
            if (authenticationContext == null) {
                getToken();
            }

        } catch (Exception e) {
            LOG.error("Unexpected error:", e);
        }
    }

    public AuthenticationResponseAndConversationContext getAuthContext() {
        return authenticationContext;
    }

    public ConversationContext getConversationContext() {
        return authenticationContext.getConversationContext();
    }

    protected void getToken() {

        LOG.info("Getting a new Token (v3) as {}", igUsername);

        CreateSessionV3Request authRequest = new CreateSessionV3Request();
        authRequest.setIdentifier(igUsername);
        authRequest.setPassword(igPassword);

        authenticationContext = restApi.createSessionV3(authRequest, igAppKey);
    }

    protected void refreshToken() throws Exception {

        LOG.info("Refreshing session for {}", igUsername);

        ConversationContext convoContext = authenticationContext.getConversationContext();
        ConversationContextV3 contextV3 = (ConversationContextV3) convoContext;

        ConversationContextV3 newContextV3 = new ConversationContextV3(restApi.refreshSessionV1(contextV3,
                        RefreshSessionV1Request.builder().refresh_token(contextV3.getRefreshToken()).build()),
                        contextV3.getAccountId(), contextV3.getApiKey());
        authenticationContext.setConversationContext(newContextV3);
    }
}

