package net.bugorfeature.moneyshot.history;

import org.apache.http.client.HttpClient;
import org.h2.server.web.WebServlet;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Profile;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.databind.ObjectMapper;

/**
 *
 * Main SpringBoot application class
 *
 * @author Andy Geach
 */
@SpringBootApplication(scanBasePackages = {"net.bugorfeature.moneyshot.history", "com.iggroup.webapi.samples"})
@EnableJpaRepositories
@EnableScheduling
public class History {

    public static void main(String[] args) {
        SpringApplication.run(History.class, args);
    }

    @Value("${ig.api.domain.URL}")
    private String serviceUrl;

    @Bean
    public ObjectMapper objectMapper() {
        return new ObjectMapper();
    }

    @Bean
    public HttpComponentsClientHttpRequestFactory requestFactory() {
        HttpComponentsClientHttpRequestFactory factory = new HttpComponentsClientHttpRequestFactory();
        factory.setConnectTimeout(10000);
        factory.setReadTimeout(10000);
        return factory;
    }

    @Bean
    public RestTemplate restTemplate() {
        RestTemplate template = new RestTemplate();
        template.setRequestFactory(requestFactory());
        return template;
    }

    @Bean
    @Profile("!postgresql & !heroku")
    ServletRegistrationBean h2servletRegistration() {
        ServletRegistrationBean<WebServlet> registrationBean = new ServletRegistrationBean<>(new WebServlet());
        registrationBean.addUrlMappings("/h2-console/*");
        return registrationBean;
    }

    @Bean
    public HttpClient httpClient() {
        return requestFactory().getHttpClient();
    }

    @Bean
    public String igApiDomainURL() {
        return serviceUrl;
    }

    @Bean
    public History application() {
        return new History();
    }

}

