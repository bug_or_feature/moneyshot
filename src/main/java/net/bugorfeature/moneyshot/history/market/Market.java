/*
 * Copyright 2019 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.bugorfeature.moneyshot.history.market;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

import org.springframework.util.Assert;

import lombok.Data;

import net.bugorfeature.moneyshot.history.price.PriceRangeForTime;

/**
 * Represents a betting market in IG
 *
 * @author Andy Geach
 */
@Data
@Entity(name = "market")
@Table(indexes = {
        @Index(columnList = "epic"),
        @Index(columnList = "instrumentType"),
        @Index(columnList = "market_name"),
        @Index(columnList = "expiry"),
        @Index(columnList = "last_open")
    },
    uniqueConstraints = {
        @UniqueConstraint(columnNames = {"market_name", "expiry"})
    }
)
public class Market implements Serializable {

    @Id
    @Column
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false, unique = true, length = 30)
    private String epic;

    @Column(name = "market_name", nullable = false, length = 30)
    private String name;

    @Column(nullable = false, length = 20)
    private String instrumentType;

    @Column(nullable = false, length = 10)
    private String expiry;

    @Column(nullable = false)
    private Integer delay;

    @Column(name = "last_open")
    private LocalDate lastOpenDate;

    @OneToMany(mappedBy = "market", cascade = CascadeType.ALL, orphanRemoval = true)
    private Set<PriceRangeForTime> priceRangeForTimes = new HashSet<>();

    protected Market() {
    }

    public Market(String epic, String name, String instrumentType, String expiry, Integer delay) {

        Assert.hasLength(epic, "epic must not be empty");
        Assert.hasLength(name, "name must not be empty");
        Assert.notNull(instrumentType, "instrumentType must not be null");
        Assert.hasLength(expiry, "expiry must not be empty");
        Assert.notNull(delay, "Delay must not be null");

        this.name = name;
        this.epic = epic;
        this.instrumentType = instrumentType;
        this.expiry = expiry;
        this.delay = delay;
    }

    public void addPrices(PriceRangeForTime prices) {
        priceRangeForTimes.add(prices);
        prices.setMarket(this);
    }
}
