/*
 * Copyright 2019 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.bugorfeature.moneyshot.history.market;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import net.bugorfeature.moneyshot.history.price.PricesRepository;

/**
 * Service implementation to provide and set up Market related data
 *
 * @author Andy Geach
 */
@Component("marketService")
@Transactional
public class MarketServiceImpl implements MarketService {

    private MarketRepository marketRepository;

    private PricesRepository pricesRepository; // NOSONAR

    @Autowired
    MarketServiceImpl(MarketRepository marketRepository, PricesRepository pricesRepository) {
        this.marketRepository = marketRepository;
        this.pricesRepository = pricesRepository;
    }

    @Override
    public Market getMarket(String epic) {
        return marketRepository.findByEpic(epic);
    }

    @Override
    public Market save(Market market) {
        return marketRepository.save(market);
    }

    /*@Override
    public PriceRangeForTime addPrices(Market market, LocalDateTime snapshotTime, PriceDetails priceDetails) {

        PriceRangeForTime prices = pricesRepository.findByMarketAndSnapshotTime(market, snapshotTime);

        if (prices == null) {
            prices = new PriceRangeForTime(market, snapshotTime, priceDetails);

        } else {
            prices.setOpenPriceBid(priceDetails.getOpenPriceBid());
            prices.setOpenPriceAsk(priceDetails.getOpenPriceAsk());
            prices.setClosePriceAsk(priceDetails.getClosePriceAsk());
            prices.setClosePriceAsk(priceDetails.getClosePriceAsk());
            prices.setHighPriceBid(priceDetails.getHighPriceBid());
            prices.setHighPriceAsk(priceDetails.getHighPriceAsk());
            prices.setLowPriceBid(priceDetails.getLowPriceBid());
            prices.setLowPriceAsk(priceDetails.getLowPriceAsk());
        }

        return pricesRepository.save(prices);
    }

    @Override
    public PriceRangeForTime getPricesForDay(Market market, LocalDateTime dateTime) {
        return pricesRepository.findByMarketEpicAndSnapshotTime(market.getEpic(), dateTime);
    }*/
}
