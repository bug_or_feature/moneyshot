/*
 * Copyright 2019 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.bugorfeature.moneyshot.history.price;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import net.bugorfeature.moneyshot.history.market.Market;

/**
 * Represents a row of price data for a particular point in time, for a market
 *
 * @author Andy Geach
 */
@Getter
@Setter
@EqualsAndHashCode(exclude = {"market"})
@Entity
@Table(
    name = "price_time",
    uniqueConstraints = @UniqueConstraint(columnNames={"market_id", "snapshotTime"}),
    indexes = {
        @Index(columnList = "snapshotTime")
    }
)
public class PriceRangeForTime implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "market_id")
    private Market market;

    @Column(nullable = false)
    private LocalDateTime snapshotTime;

    @Column(nullable = false, precision = 12, scale = 5)
    private BigDecimal openPriceBid;

    @Column(nullable = false, precision = 12, scale = 5)
    private BigDecimal openPriceAsk;

    @Column(nullable = false, precision = 12, scale = 5)
    private BigDecimal closePriceBid;

    @Column(nullable = false, precision = 12, scale = 5)
    private BigDecimal closePriceAsk;

    @Column(nullable = false, precision = 12, scale = 5)
    private BigDecimal highPriceBid;

    @Column(nullable = false, precision = 12, scale = 5)
    private BigDecimal highPriceAsk;

    @Column(nullable = false, precision = 12, scale = 5)
    private BigDecimal lowPriceBid;

    @Column(nullable = false, precision = 12, scale = 5)
    private BigDecimal lowPriceAsk;

    private Long lastTradedVolume;

    public PriceRangeForTime() {
    }

    public PriceRangeForTime(Market market, LocalDateTime snapshotTime, PriceDetails priceDetails) {

        this.market = market;
        this.snapshotTime = snapshotTime;

        this.openPriceBid = priceDetails.getOpenPriceBid();
        this.openPriceAsk = priceDetails.getOpenPriceAsk();
        this.closePriceBid = priceDetails.getClosePriceBid();
        this.closePriceAsk = priceDetails.getClosePriceAsk();
        this.highPriceBid = priceDetails.getHighPriceBid();
        this.highPriceAsk = priceDetails.getHighPriceAsk();
        this.lowPriceBid = priceDetails.getLowPriceBid();
        this.lowPriceAsk = priceDetails.getLowPriceAsk();

        this.lastTradedVolume = priceDetails.getLastTradedVolume();
    }

    public void setMarket(Market market) {
        this.market = market;
        market.getPriceRangeForTimes().add(this);
    }
}
