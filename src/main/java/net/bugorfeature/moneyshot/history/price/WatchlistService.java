package net.bugorfeature.moneyshot.history.price;

import java.time.LocalDate;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import com.iggroup.webapi.samples.client.RestAPI;
import com.iggroup.webapi.samples.client.rest.dto.watchlists.getWatchlistByWatchlistIdV1.GetWatchlistByWatchlistIdV1Response;
import com.iggroup.webapi.samples.client.rest.dto.watchlists.getWatchlistByWatchlistIdV1.MarketStatus;
import com.iggroup.webapi.samples.client.rest.dto.watchlists.getWatchlistByWatchlistIdV1.MarketsItem;
import com.iggroup.webapi.samples.client.rest.dto.watchlists.getWatchlistsV1.GetWatchlistsV1Response;
import com.iggroup.webapi.samples.client.rest.dto.watchlists.getWatchlistsV1.WatchlistsItem;

import net.bugorfeature.moneyshot.history.common.AbstractIgService;
import net.bugorfeature.moneyshot.history.common.IgServiceException;
import net.bugorfeature.moneyshot.history.market.Market;
import net.bugorfeature.moneyshot.history.market.MarketService;
import net.bugorfeature.moneyshot.history.session.IgSessionService;

/**
 * Gets Watchlist information from IG
 *
 * @author Andy Geach
 */
@Service("watchlistService")
public class WatchlistService extends AbstractIgService {

    private static final Logger LOG = LoggerFactory.getLogger(WatchlistService.class);

    @Value("${moneyshot.watchlist}")
    private String watchlistName;

    private IgSessionService sessionService;

    @Autowired
    public WatchlistService(RestAPI restApi, IgSessionService sessionService, MarketService marketService) {
        super(restApi, marketService);
        this.sessionService = sessionService;
    }

    @Scheduled(cron = "${moneyshot.service.watchlist.cron}", zone = "Europe/London")
    @Override
    public void service() {

        try {
            doWork();

        } catch (Exception e) {
            LOG.error("Unexpected error:", e);
        }
    }

    private void doWork() throws IgServiceException {

        //Assert.notNull(sessionService, "session service must not be null");
        //Assert.notNull(sessionService.getConversationContext(), "conservationContext must not be null");

        GetWatchlistsV1Response resp = null;
        try {
            resp = restApi.getWatchlistsV1(sessionService.getConversationContext());
        } catch (Exception e) {
            throw new IgServiceException(e);
        }

        LOG.info("GetWatchlistsV1Response: market count {}", resp.getWatchlists().size());

        String id = null;

        for (WatchlistsItem item : resp.getWatchlists()) {
            LOG.debug("Watchlists: {} {} ", item.getId(), item.getName());
            if (item.getName().equals(watchlistName)) {
                id = item.getId();
                break;
            }
        }

        if (id != null) {

            GetWatchlistByWatchlistIdV1Response watchlistResp = null;
            try {
                watchlistResp = restApi.getWatchlistByWatchlistIdV1(sessionService.getConversationContext(), id);
            } catch (Exception e) {
                throw new IgServiceException(e);
            }

            for (MarketsItem item : watchlistResp.getMarkets()) {
                saveMarket(item);
            }

        } else {
            LOG.error("No watchlist found with name '{}'", watchlistName);
        }
    }

    private void saveMarket(MarketsItem item) {

        LOG.info("Market: {} {} {} {}", item.getInstrumentName(), item.getEpic(), item.getExpiry(), item.getDelayTime());

        Market market = marketService.getMarket(item.getEpic());
        if (market == null) {
            market = new Market(item.getEpic(), item.getInstrumentName(), item.getInstrumentType().name(), item.getExpiry(), item.getDelayTime());

        } else {
           market.setEpic(item.getEpic());
           market.setName(item.getInstrumentName());
           market.setInstrumentType(item.getInstrumentType().name());
           market.setExpiry(item.getExpiry());
           market.setDelay(item.getDelayTime());
        }

        if (item.getMarketStatus().equals(MarketStatus.TRADEABLE)) {
            market.setLastOpenDate(LocalDate.now());
        }

        marketService.save(market);
    }

    public void setWatchlistName(String watchlistName) {
        this.watchlistName = watchlistName;
    }
}

