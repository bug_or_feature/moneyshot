/*
 * Copyright 2019 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.bugorfeature.moneyshot.history.price;

import java.math.BigDecimal;

import lombok.Builder;
import lombok.Data;

/**
 * Helper class for managing market prices
 *
 * @author Andy Geach
 */
@Data
@Builder
public class PriceDetails {

    private static final long serialVersionUID = 1L;

    private BigDecimal openPriceBid;

    private BigDecimal openPriceAsk;

    private BigDecimal closePriceBid;

    private BigDecimal closePriceAsk;

    private BigDecimal highPriceBid;

    private BigDecimal highPriceAsk;

    private BigDecimal lowPriceBid;

    private BigDecimal lowPriceAsk;

    private Long lastTradedVolume;

    public PriceDetails(BigDecimal openPriceBid, // NOSONAR
                        BigDecimal openPriceAsk,
                        BigDecimal closePriceBid,
                        BigDecimal closePriceAsk,
                        BigDecimal highPriceBid,
                        BigDecimal highPriceAsk,
                        BigDecimal lowPriceBid,
                        BigDecimal lowPriceAsk,
                        Long lastTradedVolume) {

        this.openPriceBid = openPriceBid;
        this.openPriceAsk = openPriceAsk;
        this.closePriceBid = closePriceBid;
        this.closePriceAsk = closePriceAsk;
        this.highPriceBid = highPriceBid;
        this.highPriceAsk = highPriceAsk;
        this.lowPriceBid = lowPriceBid;
        this.lowPriceAsk = lowPriceAsk;
        this.lastTradedVolume = lastTradedVolume;
    }
}
