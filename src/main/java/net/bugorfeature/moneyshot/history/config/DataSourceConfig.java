/*
 * Copyright 2019 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.bugorfeature.moneyshot.history.config;

import java.net.URI;
import java.net.URISyntaxException;

import org.postgresql.ds.PGSimpleDataSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

/**
 * Real database config.
 *
 * This class is in a separate package so that it can be excluded from coverage checks and reports
 *
 * @author Andy Geach
 */
@Configuration
public class DataSourceConfig {

    @Bean
    @Profile("postgresql")
    public PGSimpleDataSource dataSource() throws URISyntaxException {
        return buildDataSource();
    }

    @Bean
    @Profile("heroku")
    public PGSimpleDataSource herokuDataSource() throws URISyntaxException {
        PGSimpleDataSource basicDataSource = buildDataSource();
        basicDataSource.setSslMode("require");
        return basicDataSource;
    }

    /**
     * DATABASE_URL in Heroku has format:
     *
     * <pre class="code">
     * postgres://<username>:<password>@<host>/<dbname>
     * </pre>
     *
     * but Postgres JDBC driver uses
     *
     * <pre class="code">
     * jdbc:postgresql://<host>:<port>/<dbname>?sslmode=require&user=<username>&password=<password>
     * </pre>
     *
     * @return
     * @throws URISyntaxException
     * @see https://devcenter.heroku.com/articles/heroku-postgresql#connecting-in-java
     */
    private PGSimpleDataSource buildDataSource() throws URISyntaxException {
        URI dbUri = new URI(System.getenv("DATABASE_URL"));

        String[] creds = dbUri.getUserInfo().split(":");
        String dbUrl = "jdbc:postgresql://" + dbUri.getHost() + ':' + dbUri.getPort() + dbUri.getPath();

        PGSimpleDataSource basicDataSource = new PGSimpleDataSource();
        basicDataSource.setUrl(dbUrl);
        basicDataSource.setUser(creds[0]);
        basicDataSource.setPassword(creds[1]);

        return basicDataSource;
    }

}
