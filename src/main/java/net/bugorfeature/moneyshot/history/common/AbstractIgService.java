package net.bugorfeature.moneyshot.history.common;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.iggroup.webapi.samples.client.RestAPI;

import net.bugorfeature.moneyshot.history.market.MarketService;

/**
 * Abstract service for getting data from IG via their Rest API
 *
 * @author Andy Geach
 */
@Component
public abstract class AbstractIgService {

    protected final RestAPI restApi;

    protected final MarketService marketService;

    @Autowired
    public AbstractIgService(RestAPI restApi, MarketService marketService) {
        this.restApi = restApi;
        this.marketService = marketService;
    }

    protected abstract void service() throws IgServiceException;

}

