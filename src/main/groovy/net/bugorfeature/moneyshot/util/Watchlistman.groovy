package net.bugorfeature.moneyshot.util

import com.iggroup.webapi.samples.client.RestAPI
import com.iggroup.webapi.samples.client.rest.AuthenticationResponseAndConversationContext
import com.iggroup.webapi.samples.client.rest.ConversationContext
import com.iggroup.webapi.samples.client.rest.ConversationContextV3
import com.iggroup.webapi.samples.client.rest.dto.markets.getMarketDetailsBySearchTermV1.GetMarketDetailsBySearchTermV1Response
import com.iggroup.webapi.samples.client.rest.dto.markets.getMarketDetailsBySearchTermV1.MarketsItem
import com.iggroup.webapi.samples.client.rest.dto.session.createSessionV3.CreateSessionV3Request
import com.iggroup.webapi.samples.client.rest.dto.session.refreshSessionV1.RefreshSessionV1Request
import com.iggroup.webapi.samples.client.rest.dto.watchlists.createWatchlistV1.CreateWatchlistV1Request
import com.iggroup.webapi.samples.client.rest.dto.watchlists.createWatchlistV1.CreateWatchlistV1Response
import com.iggroup.webapi.samples.client.rest.dto.watchlists.deleteWatchlistV1.DeleteWatchlistV1Response
import com.iggroup.webapi.samples.client.rest.dto.watchlists.getWatchlistsV1.GetWatchlistsV1Response
import com.iggroup.webapi.samples.client.rest.dto.watchlists.getWatchlistsV1.WatchlistsItem
import com.iggroup.webapi.samples.client.rest.dto.watchlists.updateWatchlistMarketV1.UpdateWatchlistMarketV1Request
import com.iggroup.webapi.samples.client.rest.dto.watchlists.updateWatchlistMarketV1.UpdateWatchlistMarketV1Response
import org.apache.http.client.HttpClient
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.CommandLineRunner
import org.springframework.boot.SpringApplication
import org.springframework.boot.WebApplicationType
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.context.annotation.Bean
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory
import org.springframework.web.client.RestTemplate

/**
 * Bulk Watchlist populator
 *
 * @author Andy Geach 
 */
@SpringBootApplication(scanBasePackages = ["com.iggroup.webapi.samples"])
class Watchlistman implements CommandLineRunner {

    static final Logger LOG = LoggerFactory.getLogger(Watchlistman.class)

    @Value('${ig.api.domain.URL}')
    String serviceUrl

    @Value('${spring.ig.username}')
    String igUsername

    @Value('${spring.ig.password}')
    String igPassword

    @Value('${spring.ig.appKey}')
    String igAppKey

    @Autowired
    RestAPI restApi

    AuthenticationResponseAndConversationContext authenticationContext

    boolean createNew = true

    String watchlistName = 'small_cap'

    static void main(String[] args) {
        SpringApplication app = new SpringApplication()
        app.setWebApplicationType(WebApplicationType.NONE)
        app.run(Watchlistman.class, args)
    }

    @Override
    void run(String... args) throws Exception {

        LOG.info("running")

        login()

        String[] tickers = readTickersCSV('src/main/resources/input/small_cap.csv')
        //String[] tickers = readTickersXLSX('src/main/resources/input/microcap_repaired.xlsx', 1)

        deleteWatchlist(watchlistName)

        String watchlistId = createWatchlist(watchlistName)

        addEpicsToWatchlist(tickers, watchlistId)

        LOG.info("finished")

    }

    private void deleteWatchlist(String watchlistName) {

        String watchlistId = findWatchlistIdForName(watchlistName)

        if (watchlistId) {
            DeleteWatchlistV1Response deleteWatchlistResponse

            try {
                deleteWatchlistResponse = restApi.deleteWatchlistV1(authenticationContext.getConversationContext(), watchlistId)
                LOG.info("Watchlist '${watchlistName}' deleted")

            } catch (Exception e) {
                LOG.error("Problem deleting watchlist '${watchlistName}': ${e.toString()}")
            }

        } else {
            LOG.info("No watchlist found with name '${watchlistName}'")
        }
    }


    private String createWatchlist(String watchlistName) {

        CreateWatchlistV1Request createWatchlistRequest = new CreateWatchlistV1Request()
        createWatchlistRequest.name = watchlistName

        CreateWatchlistV1Response response

        try {
            response = restApi.createWatchlistV1(authenticationContext.getConversationContext(), createWatchlistRequest)
            LOG.info("Watchlist with name '${watchlistName}' created with status '${response.status.toString()}'. ID ${response.watchlistId}")

        } catch (Exception e) {
            LOG.error("Problem creating watchlist '${watchlistName}': ${e.toString()}")
        }

        return response.watchlistId
    }


    private String findWatchlistIdForName(String watchlistName) {

        GetWatchlistsV1Response response = null

        try {
            response = restApi.getWatchlistsV1(authenticationContext.getConversationContext())

            LOG.debug("Watchlist find count: ${response.getWatchlists().size()}")

            String id = null

            for (WatchlistsItem item : response.getWatchlists()) {
                LOG.debug("Watchlists: name ${item.getId()} '${item.getName()}'")
                if (item.getName() == watchlistName) {
                    return item.getId()
                }
            }

        } catch (Exception e) {
            LOG.warn("Problem finding watchlist '${item.getName()}': ${e.message}")
        }

        return null
    }


    private void addEpicsToWatchlist(String[] tickers, String watchlistId) {

        int count = 0
        for (String ticker : tickers) {

            if (count > 60) {
                LOG.info("Max requests per minute reached. Waiting...")
                Thread.sleep(60000)
                LOG.info("Wait over")
                count = 0
                login()
            }

            String epic = findEpicForTicker(ticker)
            count++

            addTickerToWatchlist(watchlistId, epic)
            count++
        }
    }

    private void addTickerToWatchlist(String watchlistId, String epic) {

        UpdateWatchlistMarketV1Request request = new UpdateWatchlistMarketV1Request()
        request.setEpic(epic)

        UpdateWatchlistMarketV1Response response

        try {
            response = restApi.updateWatchlistMarketV1(authenticationContext.getConversationContext(), watchlistId, request)

        } catch (Exception e) {
            LOG.error("Problem updating watchlist '${watchlistId}' with epic ${epic}: ${e.toString()}")

        }
    }

    private String findEpicForTicker(String ticker) {
        try {
            GetMarketDetailsBySearchTermV1Response response = restApi.getMarketDetailsBySearchTermV1(authenticationContext.getConversationContext(), ticker)

            MarketsItem market = response?.markets.find {
                it.expiry = "DFB"
            }

            if (market) {
                LOG.info("Found epic '${market.epic}' for ticker '${ticker}'")
                return market.epic
            }
        } catch (Exception e) {
            LOG.error("Problem searching for ${ticker}: ${e.toString()}")
        }

        return null
    }


    /*private String[] readTickersXLSX(String inputFile, int column) {

        def tickers = []

        FileInputStream file = new FileInputStream(new File(inputFile))
        Workbook workbook = new XSSFWorkbook(file)

        Sheet sheet = workbook.getSheetAt(0)

        for (Row row : sheet) {
            int i = 0
            row.eachWithIndex { Cell cell, index ->
                if (index == column) {
                    String ticker = row.getCell(column).getRichStringCellValue().getString()
                    LOG.debug("XLSX ticker ${ticker} found ")

                    tickers.add(ticker)
                }
            }
        }

        LOG.info("ticker count from CSV: ${tickers.size()}")

        return tickers
    }*/

    private String[] readTickersCSV(String inputFile) {

        def tickers = []

        File file = new File(inputFile)
        file.withReader { Reader reader ->
            reader.eachLine { String line ->
                String[] splits = line.split('\t')

                tickers.add(splits[1])
                LOG.debug("CSV ticker found: ${splits[1]}")
            }
        }

        LOG.info("File '${file.name}' parsed, ticker count: ${tickers.size()}")

        return tickers
    }

    private void login() {

        try {
            if (authenticationContext == null) {
                getToken()
            } else {
                refreshToken()
            }

        } catch (Exception e) {
            LOG.error("Problem logging in: ${e.getMessage()}")

            LOG.info("Resetting auth context")
            authenticationContext = null

        }
    }

    private void getToken() {

        LOG.info("Getting a new Token (v3) as ${igUsername}")

        CreateSessionV3Request authRequest = new CreateSessionV3Request()
        authRequest.setIdentifier(igUsername)
        authRequest.setPassword(igPassword)

        authenticationContext = restApi.createSessionV3(authRequest, igAppKey)
    }

    private void refreshToken() throws Exception {

        LOG.info("Refreshing session for ${igUsername}")

        ConversationContext convoContext = authenticationContext.getConversationContext()
        ConversationContextV3 contextV3 = (ConversationContextV3) convoContext

        ConversationContextV3 newContextV3 = new ConversationContextV3(restApi.refreshSessionV1(contextV3,
                RefreshSessionV1Request.builder().refresh_token(contextV3.getRefreshToken()).build()),
                contextV3.getAccountId(), contextV3.getApiKey())
        authenticationContext.setConversationContext(newContextV3)
    }

    @Bean
    RestTemplate restTemplate() {
        RestTemplate template = new RestTemplate()
        template.setRequestFactory(requestFactory())
        return template
    }

    @Bean
    HttpComponentsClientHttpRequestFactory requestFactory() {
        HttpComponentsClientHttpRequestFactory factory = new HttpComponentsClientHttpRequestFactory()
        factory.setConnectTimeout(10000)
        factory.setReadTimeout(10000)
        return factory
    }

    @Bean
    HttpClient httpClient() {
        return requestFactory().getHttpClient()
    }
}
