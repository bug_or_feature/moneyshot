/*
 * Copyright 2019 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.bugorfeature.moneyshot.history.price;

import java.io.IOException;
import java.io.StringWriter;

import org.apache.commons.io.IOUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.client.MockRestServiceServer;
import org.springframework.web.client.RestTemplate;

import com.iggroup.webapi.samples.client.RestAPI;
import com.iggroup.webapi.samples.client.rest.ConversationContextV3;

import net.bugorfeature.moneyshot.history.market.Market;
import net.bugorfeature.moneyshot.history.market.MarketService;
import net.bugorfeature.moneyshot.history.session.IgSessionService;

import static net.bugorfeature.moneyshot.history.TestUtils.readFileIntoString;
import static org.hamcrest.Matchers.endsWith;
import static org.mockito.ArgumentMatchers.any;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.requestTo;
import static org.springframework.test.web.client.response.MockRestResponseCreators.withBadRequest;
import static org.springframework.test.web.client.response.MockRestResponseCreators.withSuccess;

/**
 * Unit test for WatchlistService
 *
 * @author Andy Geach
 */
@RunWith(SpringRunner.class)
public class WatchlistServiceTest {

    private static final Logger log = LoggerFactory.getLogger(WatchlistServiceTest.class);

    WatchlistService service;

    RestTemplate restTemplate;

    MockRestServiceServer server;

    @MockBean
    private MarketService mockMarketService;

    @MockBean
    private IgSessionService sessionService;

    @Before
    public void setup() {

        HttpComponentsClientHttpRequestFactory factory = new HttpComponentsClientHttpRequestFactory();

        restTemplate = new RestTemplate();
        restTemplate.setRequestFactory(factory);

        server = MockRestServiceServer.createServer(restTemplate);

        RestAPI restAPI = new RestAPI();
        restAPI.setRestTemplate(restTemplate);
        restAPI.setIgApiDomainURL("http://fake_url/whatever");

        service = new WatchlistService(restAPI, sessionService, mockMarketService);
        service.setWatchlistName("watchlist");
    }

    @Test
    public void watchlistListException() {

        this.server.expect(requestTo(endsWith("/watchlists"))).andRespond(withBadRequest());

        this.service.service();

        server.verify();
        Mockito.verify(mockMarketService, Mockito.times(0)).save(any(Market.class));
    }

    @Test
    public void watchlistListNoMatch() {

        this.server.expect(requestTo(endsWith("/watchlists")))
                .andRespond(withSuccess(readFileIntoString("watchlistListNoMatch.json"), MediaType.APPLICATION_JSON));

        this.service.service();

        server.verify();
        Mockito.verify(mockMarketService, Mockito.times(0)).save(any(Market.class));
    }

    @Test
    public void watchlistException() {

        this.server.expect(requestTo(endsWith("/watchlists")))
                .andRespond(withSuccess(readFileIntoString("watchlistList.json"), MediaType.APPLICATION_JSON));

        this.server.expect(requestTo(endsWith("/watchlists/2"))).andRespond(withBadRequest());

        this.service.service();

        server.verify();
        Mockito.verify(mockMarketService, Mockito.times(0)).save(any(Market.class));
    }

    @Test
    public void happyPathKnownMarket() {

        this.server.expect(requestTo(endsWith("/watchlists")))
                .andRespond(withSuccess(readFileIntoString("watchlistList.json"), MediaType.APPLICATION_JSON));

        this.server.expect(requestTo(endsWith("/watchlists/2")))
                .andRespond(withSuccess(readFileIntoString("watchlist.json"), MediaType.APPLICATION_JSON));

        ConversationContextV3 convoContext = new ConversationContextV3(null, "", "");
        Market testMarket = new Market("epic_3", "FAKE_NAME", "EQUITIES", "MAR-2019", 100);

        Mockito.when(mockMarketService.getMarket(any(String.class))).thenReturn(testMarket);

        try {
            this.service.service();
        } catch (Exception e) {
            log.error("Problem: " + e);
        }

        server.verify();
        Mockito.verify(mockMarketService, Mockito.times(2)).save(any(Market.class));
    }


    @Test
    public void happyPathUnknownMarket() {

        this.server.expect(requestTo(endsWith("/watchlists")))
                .andRespond(withSuccess(readFileIntoString("watchlistList.json"), MediaType.APPLICATION_JSON));

        this.server.expect(requestTo(endsWith("/watchlists/2")))
                .andRespond(withSuccess(readFileIntoString("watchlist.json"), MediaType.APPLICATION_JSON));

        Mockito.when(mockMarketService.getMarket(any(String.class))).thenReturn(null);

        try {
            this.service.service();
        } catch (Exception e) {
            log.error("Problem: " + e);
        }

        server.verify();
        Mockito.verify(mockMarketService, Mockito.times(2)).save(any(Market.class));
    }
}
