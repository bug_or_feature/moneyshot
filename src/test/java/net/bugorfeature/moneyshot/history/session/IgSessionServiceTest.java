/*
 * Copyright 2019 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.bugorfeature.moneyshot.history.session;

import java.io.IOException;
import java.io.StringWriter;

import org.apache.commons.io.IOUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.MediaType;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.client.MockRestServiceServer;
import org.springframework.web.client.RestTemplate;

import com.iggroup.webapi.samples.client.RestAPI;
import com.iggroup.webapi.samples.client.rest.AuthenticationResponseAndConversationContext;
import com.iggroup.webapi.samples.client.rest.ConversationContextV3;

import static net.bugorfeature.moneyshot.history.TestUtils.readFileIntoString;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.endsWith;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.requestTo;
import static org.springframework.test.web.client.response.MockRestResponseCreators.withBadRequest;
import static org.springframework.test.web.client.response.MockRestResponseCreators.withSuccess;

/**
 * Unit test for IgSessionService
 *
 * @author Andy Geach
 */
@RunWith(SpringRunner.class)
public class IgSessionServiceTest {

    private static final Logger log = LoggerFactory.getLogger(IgSessionServiceTest.class);

    IgSessionService service;

    RestTemplate restTemplate;

    MockRestServiceServer server;

    RestAPI restAPI;

    @Before
    public void setup() {

        HttpComponentsClientHttpRequestFactory factory = new HttpComponentsClientHttpRequestFactory();

        restTemplate = new RestTemplate();
        restTemplate.setRequestFactory(factory);

        server = MockRestServiceServer.createServer(restTemplate);

        restAPI = new RestAPI();
        restAPI.setRestTemplate(restTemplate);
        restAPI.setIgApiDomainURL("http://fake_url/whatever");


    }

    @Test
    public void authException() {

        service = new IgSessionService(restAPI);

        this.server.expect(requestTo(endsWith("/session"))).andRespond(withBadRequest());

        this.service.service();

        server.verify();
        assertThat(service.getAuthContext()).isNull();

    }

    @Test
    public void getToken() {

        service = new IgSessionService(restAPI);

        this.server.expect(requestTo(endsWith("/session")))
            .andRespond(withSuccess(readFileIntoString("session.json"), MediaType.APPLICATION_JSON));

        this.service.service();

        server.verify();
        assertThat(service.getConversationContext()).isNotNull();

    }

    @Test
    public void checkContext() {

        service = new IgSessionService(restAPI);

        this.server.expect(requestTo(endsWith("/session")))
            .andRespond(withSuccess(readFileIntoString("session.json"), MediaType.APPLICATION_JSON));

        this.service.checkContext();

        server.verify();
        assertThat(service.getConversationContext()).isNotNull();

    }

    @Test
    public void checkContextExists() {

        ConversationContextV3 convo = new ConversationContextV3(null, "xx", "xx");

        service = new IgSessionService(restAPI, AuthenticationResponseAndConversationContext.builder()
                .conversationContext(convo)
                .build());

        this.service.checkContext();

        assertThat(service.getConversationContext()).isNotNull();
    }

    @Test
    public void checkContextException() {

        service = new IgSessionService(restAPI);

        this.server.expect(requestTo(endsWith("/session"))).andRespond(withBadRequest());

        this.service.checkContext();

        server.verify();
        assertThat(service.getAuthContext()).isNull();

    }

    @Test
    public void refreshToken() {

        ConversationContextV3 convo = new ConversationContextV3(null, "xx", "xx");

        service = new IgSessionService(restAPI, AuthenticationResponseAndConversationContext.builder()
                .conversationContext(convo)
                .build());

        this.server.expect(requestTo(endsWith("/session/refresh-token")))
                .andRespond(withSuccess(readFileIntoString("sessionRefresh.json"), MediaType.APPLICATION_JSON));

        this.service.service();

        server.verify();
        assertThat(service.getConversationContext()).isNotNull();

    }
}
