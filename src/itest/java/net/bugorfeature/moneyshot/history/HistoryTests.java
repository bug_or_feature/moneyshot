package net.bugorfeature.moneyshot.history;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.databind.ObjectMapper;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Integration test for main application
 *
 * @author Andy Geach
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class HistoryTests {

    @Autowired
    public ObjectMapper objectMapper;

    @Autowired
    public RestTemplate restTemplate;

    @Autowired
    public History application;

    @Test
    public void contextLoads() {
        assertThat(this.application).isNotNull();
        assertThat(this.restTemplate).isNotNull();
        assertThat(this.objectMapper).isNotNull();
    }

    // stupid. only exists to complete coverage
    @Test
    public void main() {
        History.main(new String[] {});
        assertThat(this.application).isNotNull();
    }
}

