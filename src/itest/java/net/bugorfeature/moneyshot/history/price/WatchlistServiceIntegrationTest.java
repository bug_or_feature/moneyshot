/*
 * Copyright 2019 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.bugorfeature.moneyshot.history.price;

import java.io.IOException;
import java.io.StringWriter;

import org.apache.commons.io.IOUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.client.MockRestServiceServer;
import org.springframework.web.client.RestTemplate;

import com.iggroup.webapi.samples.client.rest.ConversationContextV3;

import net.bugorfeature.moneyshot.history.TestUtils;
import net.bugorfeature.moneyshot.history.market.MarketRepository;
import net.bugorfeature.moneyshot.history.session.IgSessionService;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.endsWith;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.requestTo;
import static org.springframework.test.web.client.response.MockRestResponseCreators.withSuccess;


/**
 * Integration test for the WatchlistService. Full application with the IG Rest service and the IgSession services mocked
 *
 * @author Andy Geach
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class WatchlistServiceIntegrationTest {

    private static final Logger log = LoggerFactory.getLogger(WatchlistServiceIntegrationTest.class);

    @Autowired
    RestTemplate restTemplate;

    @Autowired
    MarketRepository marketRepo;

    @Autowired
    WatchlistService service;

    @MockBean
    private IgSessionService sessionService;

    MockRestServiceServer mockRestService;

    @Before
    public void setup() {
        mockRestService = MockRestServiceServer.bindTo(restTemplate).build();
    }

    @Test
    public void happyPath() {

        Mockito.when(sessionService.getConversationContext()).thenReturn(new ConversationContextV3(null, "", ""));

        long initialMarketCount = marketRepo.count();

        mockRestService.expect(requestTo(endsWith("/watchlists")))
                .andRespond(withSuccess(TestUtils.readFileIntoString("watchlistList.json"), MediaType.APPLICATION_JSON));

        mockRestService.expect(requestTo(endsWith("/watchlists/2")))
                .andRespond(withSuccess(TestUtils.readFileIntoString("watchlist.json"), MediaType.APPLICATION_JSON));

        try {
            this.service.service();
        } catch (Exception e) {
            log.error("Problem: " + e);
        }

        mockRestService.verify();

        assertThat(marketRepo.count()).isEqualTo(initialMarketCount + 2);

    }
}


