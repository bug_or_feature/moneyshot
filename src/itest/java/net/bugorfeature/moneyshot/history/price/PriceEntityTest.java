/*
 * Copyright 2019 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.bugorfeature.moneyshot.history.price;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import net.bugorfeature.moneyshot.history.market.Market;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Entity integration test for Price
 *
 * @author Andy Geach
 */
@RunWith(SpringRunner.class)
@DataJpaTest
public class PriceEntityTest {

    private static final Logger log = LoggerFactory.getLogger(PriceEntityTest.class);

    private static DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

    @Autowired
    private TestEntityManager entityManager;

    @Test
    public void saveShouldPersistData() {
        
        Market market = entityManager.find(Market.class, 1L);
        
        PriceDetails priceDetails = new PriceDetails(
            BigDecimal.valueOf(0.1),
            BigDecimal.valueOf(0.2),
            BigDecimal.valueOf(1.1),
            BigDecimal.valueOf(1.2),
            BigDecimal.valueOf(2.1),
            BigDecimal.valueOf(2.2),
            BigDecimal.valueOf(3.1),
            BigDecimal.valueOf(3.2),
            100L);
        
        PriceRangeForTime prices = new PriceRangeForTime(market, LocalDateTime.parse("2018-01-01 00:00:00", formatter), priceDetails);

        prices = this.entityManager.persistFlushFind(prices);
        assertThat(prices.getOpenPriceBid().compareTo(BigDecimal.valueOf(0.1))).isEqualTo(0);
        assertThat(prices.getOpenPriceAsk().compareTo(BigDecimal.valueOf(0.2))).isEqualTo(0);
        assertThat(prices.getClosePriceBid().compareTo(BigDecimal.valueOf(1.1))).isEqualTo(0);
        assertThat(prices.getClosePriceAsk().compareTo(BigDecimal.valueOf(1.2))).isEqualTo(0);
        assertThat(prices.getHighPriceBid().compareTo(BigDecimal.valueOf(2.1))).isEqualTo(0);
        assertThat(prices.getHighPriceAsk().compareTo(BigDecimal.valueOf(2.2))).isEqualTo(0);
        assertThat(prices.getLowPriceBid().compareTo(BigDecimal.valueOf(3.1))).isEqualTo(0);
        assertThat(prices.getLowPriceAsk().compareTo(BigDecimal.valueOf(3.2))).isEqualTo(0);
        assertThat(prices.getLastTradedVolume()).isEqualTo(100L);
    }
}
