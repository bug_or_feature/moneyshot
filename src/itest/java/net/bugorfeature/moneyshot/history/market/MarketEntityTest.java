/*
 * Copyright 2019 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.bugorfeature.moneyshot.history.market;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import net.bugorfeature.moneyshot.history.price.PriceDetails;
import net.bugorfeature.moneyshot.history.price.PriceRangeForTime;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Entity integration test for Market
 *
 * @author Andy Geach
 */
@RunWith(SpringRunner.class)
@DataJpaTest
public class MarketEntityTest {

    private static final Logger log = LoggerFactory.getLogger(MarketEntityTest.class);

    @Autowired
    private TestEntityManager entityManager;

    @Test
    public void saveShouldPersistData() {

        Market market = new Market("epic2", "name2", "type", "dfb", 0);
        market.setLastOpenDate(LocalDate.now());
        market.addPrices(new PriceRangeForTime(market, LocalDateTime.now(),
                PriceDetails.builder()
                        .openPriceBid(BigDecimal.ONE).openPriceAsk(BigDecimal.ONE)
                        .closePriceBid(BigDecimal.ONE).closePriceAsk(BigDecimal.ONE)
                        .highPriceBid(BigDecimal.ONE).highPriceAsk(BigDecimal.ONE)
                        .lowPriceBid(BigDecimal.ONE).lowPriceAsk(BigDecimal.ONE)
                        .build()));

        market = this.entityManager.persistFlushFind(market);
        assertThat(market.getEpic()).isEqualTo("epic2");
        assertThat(market.getName()).isEqualTo("name2");
        assertThat(market.getInstrumentType()).isEqualTo("type");
        assertThat(market.getExpiry()).isEqualTo("dfb");
        assertThat(market.getDelay()).isEqualTo(0);
        assertThat(market.getLastOpenDate()).isNotNull();
    }
}
