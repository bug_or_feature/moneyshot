# Moneyshot

DIY algo trading engine. Use free/cheap financial data to programmatically make
trading/investment deals for profit.

1. Data module
2. Strategy module
3. Risk module
4. Execution module


### Data module

* Service that captures and persists instrument reference information on
markets of interest
  * timed service runs every ~15 mins, reads watchlist names from config
  * creates market instance for each market in the watch list

* Service that captures and persists historical price data on markets of interest
  * daily: runs every morning, loops over every market, and gets yesterdays
  price. active flag?

  * continuous, loops over every (active?) market grabbing missing historical
  prices, going further and further back in time, max 400 days


### Reports

* [Unit test](https://moneyshot.gitlab.io/history/test/index.html)
* [Integration test](https://moneyshot.gitlab.io/history/itest/index.html)
* [Coverage](https://moneyshot.gitlab.io/history/jacoco/index.html)
